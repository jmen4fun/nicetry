package ru.SlivUP.Rozetka;

        import com.codeborne.selenide.Condition;
        import com.codeborne.selenide.ElementsCollection;
        import com.codeborne.selenide.Selenide;
        import org.junit.Assert;
        import org.junit.Test;
        import com.codeborne.selenide.Configuration;

        import static com.codeborne.selenide.Selenide.*;
        import org.openqa.selenium.By;

public class Rozetked {

    private final String url = "https://rozetka.com.ua/notebooks/c80004/comparison/ids=28374993%2C28376177" + "/";

    @Test
    public void newRoz() {

        Configuration.browser = "chrome";
        Configuration.headless = true;

        int count1 = 0;
        int count2 = 0;

        open(url);
        ElementsCollection str = $(By.cssSelector("div.wrap .comparison-t")).$$(By.cssSelector("div.wrap .comparison-t .comparison-t-row"));
        for (int i = 0; i < str.size(); i++) {
            String text1 = str.get(i).$$(By.cssSelector("div.wrap .comparison-t .comparison-t-row .comparison-t-cell")).get(0).text().toLowerCase();
            String text2 = str.get(i).$$(By.cssSelector("div.wrap .comparison-t .comparison-t-row .comparison-t-cell")).get(1).text().toLowerCase();
            if (!text1.equals(text2)) {
                count1++;
            }
        }
        $(By.cssSelector("div#compare-menu ul.m-tabs li:nth-child(2) a")).click();
        ElementsCollection str2 = $(By.cssSelector("div.wrap .comparison-t")).$$(By.cssSelector("div.wrap .comparison-t-row[name=different]"));
        count2 = str2.size();
        Assert.assertTrue(count1 == count2);
    }
}