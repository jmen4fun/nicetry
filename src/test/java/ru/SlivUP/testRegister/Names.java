package ru.SlivUP.testRegister;


import org.junit.Test;
import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;
import org.openqa.selenium.By;
import java.util.ArrayList;

public class Names {
    @Test
    public  void variableNames(){

        String[] adminNames = {"asdfyomnft","admin1", "admin"};
        String[] firstName = {"krocs", "sdfghr1234", "Тарик", "qwerty!@#", "josef", "joseff", "josefff", "joseffff",
                "jfoseff", "jfoseeff"};
        String[] secondName = {"krocs", "krocsr", "sdfghr1234", "Тарик", "qwerty!@#", "josef", "joseff", "josefff", "joseffff",
                "jfoseff", "jfoseeff"};
        String[] loginNames ={"krocs", "krocsr", "sdfghr1234", "sdfghr1234","Тарик", "qwerty!@#", "josef", "joseff", "josefff", "joseffff",
                "jfoseff" };
        String[] passField = {"123456t", "123456t!@#", "123456", "12345t", "123456t", "123456е", "qwerty",
                " 123456t", "123 456t", "123456t "};
        String[] passConf = {"123456t", "123456t!@#", "123456", "12345t", "123456T", "123456е", "qwerty",
                " 123456t", "123 456t", "123456t "};
        String[] mailField = {"jo22@jok.jok","jo22@jok.jokrc", "jok02jok.jok", "jok01@jokjok", "авкреа@апаррпю.апопало" ,
                "jok01@jok.jok", "jok02@jok.jok", "jok031@jok.jok", "jok014@jok.jok", "jok015@jok.jok", "jok015@jok.jok"};
        registerFile registr = new registerFile();
        registr.registerInfo(adminNames, loginNames, passField, passConf, mailField, firstName,
                secondName);
    }
}

