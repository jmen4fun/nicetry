package ru.SlivUP.testRegister;

import org.junit.Test;
import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;
import org.openqa.selenium.By;
import java.util.ArrayList;

public class userRegister {
    public void usReg(String[] adminNames, String[] loginNames,
                      String[] passField, String[] passConf, String[] mailField,
                      String[] firstName, String[] secondName){
        open("http://test2.benko.t.ukrtech.info/site/login");
        //авторизация пользователя
        $(By.id("LoginForm_username")).val(loginNames[0]);
        $(By.id("LoginForm_password")).val(passField[0]);
        $(By.name("yt0")).click();
        for(int i = 0; i<1; i++){
            open("http://test2.benko.t.ukrtech.info/register");
            $(By.id("UsersRegister_username")).val(firstName[i]);
            $(By.id("UsersRegister_email")).val(mailField[i]);
            $(By.id("ProfileRegister_country__id")).selectOption("Япония");
            $(By.id("UsersRegister_password")).val(passField[i]);
            $(By.id("UsersRegister_password_confirm")).val(passConf[i]);
            $(By.name("btn_register")).click();
            $(By.name("UsersRegister_username")).scrollIntoView(true);
            screenshot("my_file_nameA&G"+ i);
        }
    }
}
