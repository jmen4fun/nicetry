package ru.SlivUP.ru.mnogopotok;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;


public class SampleRegister {

    private final String url = "http://imperial.test9.polishchiuk.tcl.ukrtech.info" + "/";

    @DataProvider(name = "Authentication", parallel = true)

    public static Object[][] credentials() {

        Object data[][] = new Object[5][4]; // устанавливаем: кол-во юзеров / размер матрицы

        for (int i = 0; i < data.length; i++){
            data[i][0] = "BloodHunter";
            data[i][1] = "BloodHunter7" + i;
            data[i][2] = "BloodHunter7" + i + "@mail.com";
            data[i][3] = "jmen4fun";
        } return data;
    }

    @Test(dataProvider = "Authentication")
    public void newRegister(String Sponsor, String Name, String Mail, String userPassword) {

        Configuration.browser = "firefox";
        Configuration.headless = true;

//============================================================Register==================================================

        open(url + "register");
        $(By.cssSelector("div._form_group #sponsor_login")).val(Sponsor); // Спонсор
        $(By.cssSelector("div._form_group #UsersRegister_username")).val(Name);
        $(By.cssSelector("div._form_group #UsersRegister_email")).val(Mail); // почта
        $(By.cssSelector("div._form_group #UsersRegister_password")).val(userPassword); // для пароля
        $(By.cssSelector("div._form_group #UsersRegister_password_confirm")).val(userPassword); //повтор пароля
        Selenide.executeJavaScript("$('#ProfileRegister_form_agree').trigger('click');"); // Флажок
        $(By.xpath("//input[@class='_button _button_orange']")).click(); // Кнопка ["Зарегестрироваться"]
        System.out.println("Register_is_Complete" );

//============================================================Login=====================================================

//        open(url + "site/login");
//        $(By.xpath("//input[@id='LoginForm_username']")).val(Name); // Логин
//        $(By.xpath("//input[@id='LoginForm_password']")).val(userPassword); // Пароль
//        $(By.xpath("//button[@class='_button _button_orange']")).click();
//        System.out.println("Login_is_Complete");

//============================================================Wallets===================================================

        $(By.xpath("//ul[@class='navigation navigation-main navigation-accordion']/li[7]")).click();
        $(By.xpath("//input[@id='FinancialPasswordForm_finpassword']")).val(userPassword);
        $(By.xpath("//input[@class='btn btn-primary']")).click();
        $(By.xpath("//input[@value='Пополнение кошелька']")).click();
        $(By.xpath("//input[@id='FinanceTransaction_amount']")).val("431871");
        $(By.cssSelector("div.col-sm-6 input.btn")).click();
        $(By.cssSelector("form input.button")).click();
        $(By.xpath("//form[@id='yw0']/div/input[@value='Эмулировать успешную оплату']")).click();
        sleep(1000);
        System.out.println("Wallet_is_Complete");

//============================================================Store_binar===============================================

        open(url + "store/catalog/investment-packages");
//            $(By.xpath("//a[@data-product-name='Клиентский пакет']")).click(); // Клиентский
        $(By.xpath("//a[@data-product-name='Soft Pack']")).click(); // soft
//            $(By.xpath("//a[@data-product-name='Business Pack']")).click(); // business
//            $(By.xpath("//a[@data-product-name='Premium Pack']")).click(); // premak
        sleep(1000);
        System.out.println("Binar_is_Complete");

//============================================================Store_invest==============================================

        open(url + "store/catalog/test");
        $(By.xpath("//a[@data-product-name='Kinder Eggs']")).click(); // Пакет Kinder Eggs
//            $(By.xpath("//a[@data-product-name='999usd']")).click(); // Пакет 999usd
        sleep(1000);
        System.out.println("invest_is_Complete");

//============================================================Order=====================================================

        open(url + "store/order");
        $(By.xpath("//*[@id='payment-content']/div/div[1]/div[1]/div/div[1]/span[2]")).click(); // выбор платёжки
        $(By.xpath("//*[@id='payment-content']/div/div[1]/div[2]/div/div/div[1]/span[2]")).click(); // доставка
        $(By.xpath("//input[@id='FinanceForm_finpassword']")).val(userPassword); //фин.парол
        $(By.xpath("//button[@class='button red submit-buy-form']")).click(); // оплатить
        sleep(1000);
//            $(By.xpath("//input[@class='button btn-block']")).click(); // Кнопка Оплатить
//            $(By.xpath("//form[@id='yw0']/div/input[@value='Эмулировать успешную оплату']")).click(); // Эмуляция платежа
        System.out.println("Order_is_Complete");
//============================================================logout====================================================

        open(url + "site/logout");
        System.out.println("-->Cycle_is_Complete_");
    }
}
