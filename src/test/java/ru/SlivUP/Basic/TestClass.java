package ru.SlivUP.Basic;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;
import com.codeborne.selenide.Configuration;
import static com.codeborne.selenide.Selenide.*;
import org.openqa.selenium.By;

public class TestClass {

    @Test
    public void test(){
        final String url = "http://teamring.test10.kornejchuk.tcl.ukrtech.info" + "/";

        String[] Sponso = {"admin"};
        String[] Name = {"jmen", "jmen4fun"};
        String[] NameBay = {""}; // массив спонсора - глубина
        String userPassword = "jmen4fun"; // пароль

        Register RegisterForm = new Register();
        Login LoginForm = new Login();

        for (int i = 0; i < 1; i++) {

            open(url + "register");
            RegisterForm.registerClass(
                    "admin",
                    "jmen" + i,
                    "jmen4fun",
                    "jmen" + i + "@gmail.com"
            );
            open(url + "site/logout");

            open(url + "site/login");
            LoginForm.LoginClass(
                    "jmen" + i,
                    "jmen4fun"
            );
            open(url + "site/logout");
        }
    }
}