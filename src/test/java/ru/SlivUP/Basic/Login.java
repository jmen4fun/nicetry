package ru.SlivUP.Basic;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;
import com.codeborne.selenide.Configuration;
import static com.codeborne.selenide.Selenide.*;
import org.openqa.selenium.By;

public class Login {

    SelenideElement

            Name = $(By.xpath("//div[@class='form-box-inp']/input[@id='LoginForm_username']")),
            Pass = $(By.xpath("//div[@class='form-box-inp']/input[@id='LoginForm_password']")),
            Button = $(By.xpath("//div[@class='box-but']/button[@class='but _but15']"));

    public Login LoginClass(String NaMe, String PasS){

        Name.val(NaMe);
        Pass.val(PasS);
        Button.click();

        return this;
    }
}
