package ru.SlivUP.Basic;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;
import com.codeborne.selenide.Configuration;
import static com.codeborne.selenide.Selenide.*;
import org.openqa.selenium.By;

public class Register {

    SelenideElement

            Sponsor = $(By.cssSelector("div.form-group #sponsor_login")),
            Name = $(By.cssSelector("div.form-group #UsersRegister_username")),
            Email = $(By.cssSelector("div.form-group #UsersRegister_email")),
            Pass = $(By.cssSelector("div.form-group #UsersRegister_password")),
            PassAgain = $(By.cssSelector("div.form-group #UsersRegister_password_confirm")),
            Checker = $(By.xpath("//div[@class='checker']")),
            Button = $(By.xpath("//button[@class='btn bg-teal btn-block btn-lg']"));

    public Register registerClass(String SponsoR, String NamE, String PasS, String EmaIl){

        Sponsor.val(SponsoR);
        Name.val(NamE);
        Email.val(NamE);
        Pass.val(PasS);
        PassAgain.val(PasS);
        Checker.click();
        Button.click();

        return this;
    }
}

